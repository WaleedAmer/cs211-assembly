#include <stdio.h>
#include <stdlib.h>
#include "mystery.h"

int num[200];

int add(int a, int b) {
	return a + b;
}

// fibonacci
int dothething(int n) {
	int temp = 0;

	if (num[n] == -1) {
		if (n == 0) {
			num[n] = 0;
		} else if (n == 1) {
			num[n] = 1;
		} else {
			num[n] = dothething(n - 1) + dothething(n - 2);
		}
	}

	temp = num[n];
	return temp;
}

int main(int argc, char **argv) {
	int i = 0;
	int n = atoi(argv[1]);

	while (i < 200) {
		num[i] = -1;
		i++;
	}

	printf("%i\n", dothething(n));
	return 0;
}
