#include "formula.h"
#include <sys/time.h>
#include "nCr.h"

bool isNumber(char number[]) {
	int i = 0;
	
	if (number[0] == '0') {
		i = 1;
	}
	
	for (; number[i] != 0; i++) {
		if (!isdigit(number[i])) {
			return false;
		}
	}

	return true;
}

int main(int argc, char *argv[]) {
	// Make sure there is only one argument
	if (argc != 2) {
		fprintf(stderr, "Error: There must be one argument.\n");
		return -1;
	}			

	// Make sure the argument is an integer
	if (!isNumber(argv[1])) {
		fprintf(stderr, "Error: The argument must be a non-negative integer.\n");
		return -1;
	}

	// Get the first argument as an integer
	int n = atoi(argv[1]);

	// Make sure n is positive
	if (n < 0) {
		fprintf(stderr, "Error: The argument must be a non-negative integer.\n");
		return -1;
	}

	// Print the beginning of the formula
	printf("(1 + x)^%d = 1", n);
	
	// Begin keeping track of the time
	struct timeval begin, end;
	gettimeofday(&begin, NULL);

	// If n equals 0 then the answer is just 1
	if (n == 0) {
		gettimeofday(&end, NULL);
		(unsigned int) end.tv_usec - begin.tv_usec;
		printf("\nTime Required = %u microsecond\n", (unsigned int) (end.tv_usec - begin.tv_usec));
		return 0;
	}

	// Print out each iteration of the formula
	int r = 1;
	for(; r <= n; r++) {
		printf(" + %d*x^%d", nCr(n, r), r);		
	}
	printf("\n");

	// Print out the time elapsed
	gettimeofday(&end, NULL);
	printf("Time Required = %u microsecond\n", (unsigned int) (end.tv_usec - begin.tv_usec));

	return 0;
}
