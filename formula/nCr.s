.globl nCr
	.type	nCr, @function
nCr:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	
	pushl	8(%ebp)
	call	Factorial
	jo	.overfl
	addl	$4, %esp
	movl	%eax, -4(%ebp)

	pushl	12(%ebp)
	call	Factorial
	jo	.overfl
	addl	$4, %esp
	movl	%eax, -8(%ebp)

	movl	8(%ebp), %eax
	subl	12(%ebp), %eax
	pushl	%eax
	call	Factorial
	jo	.overfl
	addl	$4, %esp
	movl	%eax, -12(%ebp)

	movl	-8(%ebp), %eax
	imull	-12(%ebp), %eax
	jo	.overfl
	movl	%eax, -8(%ebp)

	movl	-4(%ebp), %eax
	movl	$0, %edx
	idivl	-8(%ebp)
	
	leave
	ret
.overfl:
	movl	$0, %eax

	leave
	ret

.globl Factorial
	.type	Factorial, @function
Factorial:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp

	movl	$1, -4(%ebp)
	movl	$1, -8(%ebp)	
.fact:
	movl	-4(%ebp), %eax
	cmpl	8(%ebp), %eax
	jg	.done

	movl	-8(%ebp), %eax
	imull	-4(%ebp), %eax
	jo	.overflow
	movl	%eax, -8(%ebp)

	addl	$1, -4(%ebp)
	
	jmp	.fact
.done:
	movl	-8(%ebp), %eax
	leave
	ret
.overflow:
	movl	$0, %eax
	leave
	ret
