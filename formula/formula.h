#ifndef formula_h
#define formula_h

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

typedef int bool;
#define true 1
#define false 0

bool isNumber(char number[]);

int main(int argc, char *argv[]);

#endif
